---
title: Méthodologie
description: Méthodes et hypothèses utilisées pour la construction de l'indicateur bruit
---

#### Les seuils 

Seuil retenu en journalier d'après la règlementation de 1995 : en journalier 60dB (pour le bruit dû au trafic routier), en diurne 55dB

https://www.bruitparif.fr/la-reglementation4/#:~:text=Ainsi%20l'arr%C3%AAt%C3%A9%20du%2013,en%20fonction%20de%20la%20cylindr%C3%A9e.


* L'OMS "recommande fortement de réduire les niveaux sonores produits par le trafic routier à moins de 53 décibels (dB) Lden", donc on choisi de se baser sur un niveau de 55 dB Lden pour estimer les population soumises à un risque de "effets néfastes sur la santé"

* la loi française oblige a "limiter le niveau de bruit sous les 65 dB LAeq", donc on choisi de se baser sur un seuil de 65 dB Lden, qui diffère du seuil de détection des pb, placés à 68 dB Lden. Cela fait partie d'une certaine "duplicité" du système français : on détecte plus haut qu l'objectif de réduction. 