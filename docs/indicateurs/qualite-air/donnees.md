---
  sidebar_position: 1
  title: Données utilisées
---

### Synthèse des données existantes en qualité de l'air 

#### Cartothèque de l'INERIS et modèle Prev'air 


L’Ineris publie en ligne une cartothèque permettant de retracer l’évolution de la qualité de l’air en France, de 2000 à nos jours.

Ces cartes, représentatives de la pollution générale de l’air ambiant (pollution « de fond »), sont issues des simulations numériques, intégrant des données de mesure collectées sur le terrain. Une telle cartographie fournit des éléments pour mieux connaître l’exposition des populations et des écosystèmes et peut contribuer au suivi de l’efficacité des politiques de gestion de la qualité de l’air. .

Les données des cartes en questions sont disponibles [sur cette infercace](https://www.ineris.fr/fr/recherche-appui/risques-chroniques/mesure-prevision-qualite-air/qualite-air-france-metropolitaine)

Ces cartes sont alimentées par les données du modèle Prev'air, qui permet de produire une prédiction de la qualité de l'air dans les prochains jours, qui est mises à jour jusqu'à quelques jours après une date donnée. Effectivement, les données des stations de mesure des AASQA, entre autres, sont prises en entrée du modèle Prev'air. Les observations utilisées n'incluent que les stations urbaines, périurbaines, rurales de fond, elles ne tiennent pas comptent des stations trafic et industrielles. Ces cartes ne sont donc pas représentatives de situations de proximité de sources spécifiques (zones de trafic dense ou activités industrielles notamment).

Les données du modèle Prev'air couvrent l'intégralité du territoire métropolitain avec un maillage de 2 kilomètres. 

La documentation du modèle Prév'air est disponible [ici](https://journals.ametsoc.org/view/journals/bams/90/1/2008bams2390_1.xml) et [ici](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2007JD008761)

En modélisation annuelle, l'INERIS met à disposition les données 



#### Modélisations régionales des AASQA 

Les AASQA produisent des données localement en partenariat avec agglomérations, les départements, régions. Ces modélisations ne semblent pas accessibles facilement  

#### Stations de mesure des AASQA 

Dans chacune des 13 régions, les [AASQA](/acteurs-se/qualite-air.md) .

### Choix des données utilisés

Le projet Estim'action s'appuie pour le moment sur les données 

### Traitement des données de qualité de l'air 

Enrichissement du département : il apparaît que des géométries de Prev'air 