---
  sidebar_position: 2
  title: Méthodologie
---


### Construction de l'indicateur de population exposée à une mauvaise qualité de l'air  

:::info

Les normes des différents seuils par polluant sont définis au niveau national et européen. Il existe aussi des seuils définis par l'OMS. 

:::

#### Seuil pour les particules (PM2.5) 



En moyenne annuelle, les seuils d'exposition sont les suivants (source : [Règlementation et Normes QA version finale](https://www.ecologie.gouv.fr/sites/default/files/06_Seuils%20r%C3%A9glementaires.pdf)) : 

* Objectif de qualité (seuil FR) : 10 $\mu g/m^3$ en moyenne annuelle 
* Valeur cible pour la protection de la santé humaine (seuil FR) : 25 $\mu g/m^3$ en moyenne annuelle 
* Valeur limite 2020 pour la protection de la santé humaine (seuil UE) : 25 $\mu g/m^3$ en moyenne annuelle 

* Seuil OMS, valeur guide 2021 en moyenne annuelle : 5 $\mu g/m^3$

#### Seuil pour les particules (PM10) 



En moyenne annuelle, les seuils d'exposition sont les suivants (source : [Règlementation et Normes QA version finale](https://www.ecologie.gouv.fr/sites/default/files/06_Seuils%20r%C3%A9glementaires.pdf)) : 

* Objectif de qualité (seuil FR) : 30 $\mu g/m^3$ en moyenne annuelle 
* Valeur cible pour la protection de la santé humaine (seuil UE) : 50 $\mu g/m^3$ en moyenne annuelle à ne pas dépasser plus de 35 jours par an
* Valeur cible pour la protection de la santé humaine (seuil UE) : 40 $\mu g/m^3$ (UE) en moyenne annuelle
* Seuil d'information et de recommandation : 50 $\mu g/m^3$ en moyenne sur 24h 
* Seuil d'alerte : 80 $\mu g/m^3$ en moyenne sur 24h 

* Seuil OMS, valeur guide 2021 en moyenne annuelle : 15 $\mu g/m^3$


 Valeur guide 2021 en moyenne annuelle : 15 µg.m-3

#### Seuil pour le dioxyde d'Azote (NO2)) 



En moyenne annuelle, les seuils d'exposition sont les suivants (source : [Règlementation et Normes QA version finale](https://www.ecologie.gouv.fr/sites/default/files/06_Seuils%20r%C3%A9glementaires.pdf)) : 

* Objectif de qualité (seuil FR) : 40 $\mu g/m^3$ en moyenne annuelle 
* Valeur cible pour la protection de la santé humaine (seuil UE) : 200 $\mu g/m^3$ en moyenne horaire à ne pas dépasser plus de 18 heures par an
* Valeur cible pour la protection de la santé humaine (seuil UE) : 40 $\mu g/m^3$ (UE) en moyenne annuelle
* Niveau critique pour la protection de la végétation (NOx) (seuil UE) : 30 $\mu g/m^3$ (UE) en moyenne annuelle d'oxydes d'azote
* Seuil d'information et de recommandation (FR) : 200 $\mu g/m^3$ en moyenne horaire
* Seuil d'alerte (UE) : 400 $\mu g/m^3$ en moyenne horaire pendant 3 heures consécutives 
* ou Seuil d'alerte si 200 $\mu g/m^3$ en moyenne horaire à J-1 et à J et prévision de 200 $\mu g/m^3$ à J+1 (FR)

* Seuil OMS, valeur guide (2021): 10  $\mu g/m^3$ en moyenne annuelle


#### Sources d'information

* [Règlementation et Normes QA version finale](https://www.ecologie.gouv.fr/sites/default/files/06_Seuils%20r%C3%A9glementaires.pdf)
* [Cartothèque de l'INERIS](https://www.ineris.fr/fr/recherche-appui/risques-chroniques/mesure-prevision-qualite-air/qualite-air-france-metropolitaine)
* 