---
  sidebar_position: 1
  title: Données utilisées
---


### Population sur le territoire

La base de données population a été établie par le Cerema à partie d'une exploitation de la BDTOPO de l'IGN et des ratios de population/logement mis à disposition pour chaque commune par l'INSEE. De même, les donénes bâtiments et bâtiments sensibles ont été établies par le Cerema à partir de la BDTOPO de l'IGN et l'exploitation de différentes bases disponibles en Open Data (cf 3.1 Les bases de données d'entrée dans les résumé non technique du département)

Les Cartes de Bruit Stratégiques (CBS) produites par le Cerema permettent de placer la population Française sur le territoire. 

`C_POPULATION` (table attributaire) jointe avec `C_BATIMENT_S` (données géo) pour

**Sources de données utilisées**

- ***Base de données :*** postgres `plamade`
- ***Schéma :*** `echeance4`
- ***Table :*** `C_BATIMENT_S`
- ***Stockage actuel :*** serveur Plamade au Cerema
- ***Intérêt :*** Table géométrique des bâtiments, joignable à la population via un identifiant de bâtiment