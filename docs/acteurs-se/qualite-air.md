---
  sidebar_position: 2
  title: Qualité de l'air
---

### Les AASQA

Les Associations agréées de surveillance de la qualité de l'air sont des organismes français mesurant et étudiant la pollution atmosphérique au niveau de l'air ambiant. Elles sont agréées par le ministère de l'Écologie pour communiquer officiellement leurs résultats.