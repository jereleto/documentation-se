---
  sidebar_position: 1
  title: Développement dbt
---

Le temps pour l'exécution de la requête de jointure entre les recepteurs et les communes prend 14 minutes et 2,72 secondes depuis dbt, et cette requête prend 8 minutes et 14 secondes sur DBeaver directement : 

```sql
SELECT 
    r.*, 
    i.*
FROM (
    SELECT * 
    FROM
    {{source('recepteurs','recepteurs')}} 
    WHERE "codedept" = '033'
) r 
JOIN 
(
    SELECT * 
    FROM
    {{ref('raw_communes_epci')}}
    WHERE "admin_express_dep_code_insee" = '33'
)  i 
ON ST_INTERSECTS(r.geom, i.admin_express_commune_geometry) 
``` 


```bash
PS C:\Users\jeremie.letonnelier\Documents\Estimaction\commun-sante-environnementale> dbt run --models=mart_recepteur_communes
15:12:29  Running with dbt=1.4.5
15:12:29  Found 14 models, 0 tests, 0 snapshots, 0 analyses, 291 macros, 0 operations, 0 seed files, 5 sources, 0 exposures, 0 metrics
15:12:29  
15:12:31  Concurrency: 1 threads (target='dev')
15:12:31  
15:12:31  1 of 1 START sql table model estimaction_marts.mart_recepteur_communes ......... [RUN]
15:26:32  1 of 1 OK created sql table model estimaction_marts.mart_recepteur_communes .... [SELECT 6044862 in 840.70s]
15:26:32  
15:26:32  Finished running 1 table model in 0 hours 14 minutes and 2.72 seconds (842.72s).
```